import { useEffect, useState } from "react";
import axios from "axios";

function useSearch(query, pageNumber) {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [books, setBooks] = useState([]);
  const [hasMore, setHasMore] = useState(false);
  useEffect(() => {
    setBooks([]);
  }, [query]);
  useEffect(() => {
    setError(false);
    setLoading(true);
    let cancel;
    axios({
      method: "GET",
      url: "https://openlibrary.org/search.json",
      params: { q: query, page: pageNumber },
      cancelToken: new axios.CancelToken((c) => {
        cancel = c;
      }),
    })
      .then((res) => {
        setBooks((prevState) => {
          return [
            ...new Set([
              ...prevState,
              ...res.data.docs.map((doc) => doc.title),
            ]),
          ];
        });
        setHasMore(res.data.docs.length > 0);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);

        if (axios.isCancel(err)) return;
        else {
          console.log(err.message);
          setError(true);
        }
      });
    return () => cancel();
  }, [query, pageNumber]);

  return { loading, error, hasMore, books };
}

export default useSearch;
