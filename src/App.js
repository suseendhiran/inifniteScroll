import React, { useState, useCallback, useRef } from "react";
import useSearch from "./useSearch";
import "./App.css";

function App() {
  const [query, setQuery] = useState("");
  const [pageNumber, setPageNumber] = useState(1);

  let observer = useRef();
  const { loading, error, hasMore, books } = useSearch(query, pageNumber);

  const lastElementRef = useCallback(
    (node) => {
      console.log(node);
      if (loading) return;
      if (observer.current) {
        console.log("disconnect");
        observer.current.disconnect();
      }
      observer.current = new IntersectionObserver((entries) => {
        console.log(entries[0]);
        if (entries[0].isIntersecting && hasMore) {
          console.log("pagese change");
          setPageNumber((prevPage) => prevPage + 1);
        }
      });
      if (node) observer.current.observe(node);
      console.log(observer.current);
    },
    [loading, hasMore]
  );

  const handleSearch = (e) => {
    setQuery(e.target.value);
    setPageNumber(1);
  };

  return (
    <div className="App">
      <input type="text" value={query} onChange={handleSearch}></input>

      {books.map((book, index) => {
        if (books.length === index + 1) {
          return (
            <div ref={lastElementRef} key={book}>
              {book}
            </div>
          );
        } else {
          return <div key={book}>{book}</div>;
        }
      })}

      {loading && <div>Loading...</div>}
      {error && <div>Error</div>}
    </div>
  );
}

export default App;
